from django import forms
from .models import Donation


class DonationForm(forms.ModelForm):
    class Meta:
        model = Donation
        fields = [
            "amount",
            "location",
            "program",
            "anonymous",
        ]
        PROGRAM_CHOICES = (
            ("", "Select the Program"),
            (
                "Program 1",
                "Program 1",
            ),  # First one is the value of select option and second is the displayed value in option
            ("Program 2", "Program 2"),
            ("Program 3", "Program 3"),
            ("Program 4", "Program 4"),
        )
        ANONYMOUS = (
            (
                "YES",
                "YES",
            ),  # First one is the value of select option and second is the displayed value in option
            ("NO", "NO"),
        )
        LOCATION_CHOICES = (
            ("", "Select the Location"),
            (
                "Palu",
                "Palu",
            ),  # First one is the value of select option and second is the displayed value in option
            ("Lombok", "Lombok"),
        )

        widgets = {
            "amount": forms.NumberInput(attrs={"size": "15"}),
            "location": forms.Select(
                choices=LOCATION_CHOICES, attrs={"class": "form-control"}
            ),
            "program": forms.Select(
                choices=PROGRAM_CHOICES, attrs={"class": "form-control"}
            ),
            "anonymous": forms.Select(
                choices=ANONYMOUS, attrs={"class": "form-control"}
            ),
        }
