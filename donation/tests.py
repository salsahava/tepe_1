from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from .models import Donation


class DonationTest(TestCase):
    def test_if_is_exists(self):
        c = Client()
        self.login(c)
        response = c.get("/donation/donations/")
        self.assertEqual(response.status_code, 200)

    def test_post_donations(self):
        amount = "1000"
        location = "PALU"
        program = "PROGRAM 1"
        anonymous = "YES"

        c = Client()
        self.login(c)
        c.post(
            "/donation/donations/",
            {
                "amount": amount,
                "location": location,
                "program": program,
                "anonymous": anonymous,
            },
        )

        count = Donation.objects.all().count()
        self.assertEqual(count, 1)

    def test_tembak_json(self):
        c = Client()
        self.login(c)
        response = c.get("/donation/tembak_json/")

        test = ""
        resp = response.content.decode("utf8")
        self.assertIn(test, resp)

    def test_display_donators(self):
        Donation.objects.create(
            name="joko",
            email="widodo@gmail.com",
            amount=100,
            location="PALU",
            program="PROGRAM 1",
            anonymous="YES",
        )

        c = Client()
        self.login(c)
        c.get("/donation/display_donators/")

        count = Donation.objects.all().count()
        self.assertEqual(count, 1)

    def test_create_objects(self):
        Donation.objects.create(
            name="joko",
            email="widodo@gmail.com",
            amount=100,
            location="PALU",
            program="PROGRAM 1",
            anonymous="YES",
        )
        count = Donation.objects.all().count()
        self.assertEqual(count, 1)

    def login(self, client):
        username = "testuser"
        password = "12345678"
        email = "testuser@gmail.com"

        User.objects.create_user(username=username, email=email, password=password)
        client.login(username=username, password=password)
