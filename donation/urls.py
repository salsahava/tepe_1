from django.urls import path
from .views import donations, display_donators, tembak_json

urlpatterns = [
    path("donations/", donations, name="donations"),
    path("display_donators/", display_donators, name="display_donators"),
    path("tembak_json/", tembak_json, name="tembak_json"),
]
