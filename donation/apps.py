from django.apps import AppConfig


class DonationTestConfig(AppConfig):
    name = "donation"
