from django.db import models
from django.db.models import Sum
from django.core.validators import MinValueValidator


class Donation(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=127)
    amount = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    location = models.CharField(max_length=255)
    program = models.CharField(max_length=510)
    anonymous = models.CharField(max_length=10, default="YES")

    def set_user(self, user):
        self.name = user.username
        self.email = user.email

    @classmethod
    def sum_total_donations_by_location(cls, location):
        return cls.objects.filter(location=location).aggregate(
            total_donations=Sum("amount")
        )["total_donations"]

    class Meta:
        app_label = "donation"
