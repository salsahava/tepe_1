from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import Donation
from django.core import serializers
from django.contrib.auth.decorators import login_required
from .forms import DonationForm


response = {}


@login_required
def donations(request):
    response = {}
    if request.method == "POST":
        form = DonationForm(request.POST)

        if form.is_valid():
            new_donation = form.save(commit=False)
            new_donation.set_user(request.user)
            new_donation.save()

            return redirect("donation:display_donators")

    response["donation_form"] = DonationForm()
    return render(request, "donation/donations.html", response)


@login_required
def tembak_json(request):
    all_donation = Donation.objects.filter(email=request.user.email)
    the_json = {"all_donations": all_donation}
    the_json = serializers.serialize("json", all_donation)
    the_json = {"all_donations": the_json}
    return JsonResponse(the_json, safe=False)


@login_required
def display_donators(request):
    response = {}

    response["all_donators"] = Donation.objects.all()
    return render(request, "donation/the_donators.html", response)
