from django.urls import path
from .views import newsroom

urlpatterns = [
    path("", newsroom, name="newsroom"),
]
