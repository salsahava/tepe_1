from django.shortcuts import render
from .models import News

response = {}


def newsroom(request):
    response["newsroom"] = News.objects.all()
    return render(request, "newsroom/newsroom.html", response)
