from django.shortcuts import render
from .models import InformasiLombok, InformasiPalu
from donation.models import Donation

response = {}


def newsapp(request):
    response["palu_money"] = Donation.sum_total_donations_by_location("Palu")
    response["info_palu"] = InformasiPalu.objects.all()
    return render(request, "newsapp/palu.html", response)


def newsapp2(request):
    response["lombok_money"] = Donation.sum_total_donations_by_location("Lombok")
    response["info_lombok"] = InformasiLombok.objects.all()
    return render(request, "newsapp/lombok.html", response)
