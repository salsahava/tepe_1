from django.shortcuts import render, redirect
from django.contrib.auth import logout as logout_user

response = {}


def login(request):
    sudah_login = request.user.is_authenticated
    if not sudah_login:
        return render(request, "login/login.html", response)

    return redirect("newsroom:newsroom")


def logout(request):
    logout_user(request)
    return redirect("login:login")
