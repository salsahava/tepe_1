from django.test import Client, TestCase
from .models import Donatur


class login_Test(TestCase):
    def test_if_url_and_template_is_exists(self):
        response = Client().get("/user_login/login/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "login/login.html")

        # test buat logoutnya juga
        response = Client().get("/user_login/logout/")
        self.assertEqual(response.status_code, 302)

    def can_create_object(self):
        Donatur.objects.create(name="Igor", email="adi.vanbasten99@gmail.com")
        self.assertEqual(Donatur.objects.all().count(), 1)
