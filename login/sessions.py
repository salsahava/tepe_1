from .models import Donatur


def set_extra_data_in_session(
    backend, strategy, details, response, user=None, *args, **kwargs
):
    if backend.name == "google-oauth2":
        nama = response["name"]
        email = response["email"]
        if Donatur.objects.filter(email=email).count() == 0:
            Donatur.objects.create(name=nama, email=email)
            print(Donatur.objects.all().count())
            print("mekanisme berhasil")
            print("nama   " + response["name"])
            print("email   " + response["email"])

        strategy.session_set("id_token", response["id_token"])
        strategy.session_set("email", response["email"])
        strategy.session_set("display_name", response["name"])
        strategy.session_set("image_url", response["picture"])
