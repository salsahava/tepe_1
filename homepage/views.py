from django.shortcuts import render


response = {}


def index(request):
    return render(request, "homepage/index.html", response)
