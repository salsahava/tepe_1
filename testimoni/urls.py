from django.urls import path
from django.conf.urls import url
from .views import (
    indexs,
    testimoni_handler,
    tembak_json_testi,
    createtestimoni,
    showtestimoni,
)
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path("", indexs, name="testimoni"),
    path("write_testi/", testimoni_handler, name="write_testi"),
    path("json_testi/", tembak_json_testi, name="json_testi"),
    url(r"^createtestimoni/", createtestimoni, name="createtestimoni"),
    url(r"^showtestimoni/", showtestimoni, name="showtestimoni"),
]

urlpatterns += staticfiles_urlpatterns()
