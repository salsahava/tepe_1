from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps

from .apps import TestimoniConfig
from .views import indexs
from .models import Testimoni


class TestiAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(TestimoniConfig.name, "testimoni")
        self.assertEqual(apps.get_app_config("testimoni").name, "testimoni")


class testimoni_Test(TestCase):
    def test_if_url_and_template_are_exist(self):
        response = Client().get("/testimoni/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "testimoni/testimoni.html")

    def test_if_function_for_html_rendering_exists(self):
        found = resolve("/testimoni/")
        self.assertEqual(found.func, indexs)


class TestiModelTest(TestCase):
    def test_default_attr(self):
        testi = Testimoni()
        self.assertEqual(testi.namatestimoni, "")
        self.assertEqual(testi.testimoni, "")

    def test_testi_verbose_name_plural(self):
        self.assertEqual(str(Testimoni._meta.verbose_name_plural), "testimonis")

    def test_model_can_create_new_testi(self):
        Testimoni.objects.create(namatestimoni="coba", testimoni=":)")
        all_available_testi = Testimoni.objects.all().count()
        self.assertEqual(all_available_testi, 1)


class Formtestimoni_Test(TestCase):
    def test_success_insert_testimoni(self):
        name = "CobaCoba"
        testi = ":)"
        response = Client().post(
            "/testimoni/showtestimoni/",
            {"namatestimoni": name, "testimoni": testi},
        )

        self.assertEqual(response.status_code, 200)
