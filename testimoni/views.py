from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.http import JsonResponse
from .models import Testimoni
from django.core import serializers
import json


def indexs(request):
    return render(request, "testimoni/testimoni.html")


def testimoni_handler(request):
    if request.method == "POST":
        Testimoni.objects.create(
            namatestimoni=request.user.first_name, testimoni=request.POST["testimoni"]
        )
        return redirect("testimoni:testimoni")
    return redirect("testimoni:testimoni")


def tembak_json_testi(request):
    all_testi = Testimoni.objects.all()
    the_json = {"all_testimoni": all_testi}
    the_json = serializers.serialize("json", all_testi)
    the_json = {"all_testimoni": the_json}
    return JsonResponse(the_json, safe=False)


def createtestimoni(request):
    if request.method == "POST" and request.is_ajax():
        json_data = json.loads(request.body, encoding="UTF-8")
        Testimoni.objects.create(
            namatestimoni=json_data["namatestimoni"], testimoni=json_data["testimoni"]
        )
        return HttpResponse("New testi successfully added", status=201)
    else:
        return HttpResponse("Failed", status=405)


def showtestimoni(request):
    testimoni = Testimoni.objects.all()
    listOfTesti = []

    for testi in testimoni:
        data = {"namatestimoni": testi.namatestimoni, "testimoni": testi.testimoni}
        listOfTesti.append(data)

    return JsonResponse(listOfTesti, safe=False)
