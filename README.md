# Tepe_1

# Nama Anggota

1. Drasseta Aliyyu Darmansyah (1706074953)
2. Igor Lestin Sianipar (1706023372)
3. Salsabila Hava Qabita (1706979461)
4. Vincentius Adi Kurnianto (1706979480)

## [Link Herokuapp](https://cs-crowdfund.herokuapp.com/)

###

# Contribution

1. Vincentius Adi Kurnianto :

- All the works involving database and back-end engineering
- design test.py for most of the app
- App : register_test and donation

2. Igor Lestin Sianipar :

- Developing front end on newsroom Palu,newsroom Lombok, and Main newsroom
- App : newsroom and newsapp

3. Salsabila Hava Qabita :

- Developing front end on Homepage
- Create base.html
- App : homepage

4. Drasseta A. Darmansyah :

- Developing front end on Register and Donation page

# Pipelines Status

[![pipeline status](https://gitlab.com/vince10/tepe_1/badges/master/pipeline.svg)](https://gitlab.com/vince10/tepe_1/commits/master)

# Code Coverage

[![coverage report](https://gitlab.com/vince10/tepe_1/badges/master/coverage.svg)](https://gitlab.com/vince10/tepe_1/commits/master)
