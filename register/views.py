from django.shortcuts import render
from .forms import RegisterForm

response = {}


def index(request):
    form = RegisterForm
    response["form"] = form
    return render(request, "register/index.html", response)


def add_donatur(request):
    context = {}
    form = RegisterForm()
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            form = RegisterForm()
            context["messages"] = ["Registration Success"]
        else:
            print(form.errors)

    context["form"] = form
    return render(request, "register/index.html", context)
