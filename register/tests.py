from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Register
from .forms import RegisterForm


class RegisterTest(TestCase):
    def test_if_is_exists(self):
        response = Client().get("/register/register/")
        self.assertEqual(response.status_code, 200)

    def test_using_register_index_function(self):
        found = resolve("/register/register/")
        self.assertEqual(found.func, index)

    def test_using_register_page_template(self):
        response = Client().get("/register/register/")
        self.assertTemplateUsed(response, "register/index.html")

    def test_model_can_create_Register(self):
        Register.objects.create(
            name="Igor",
            dob="1999-01-01",
            email="ariqharyo29@gmail.com",
            password="password",
            password_verif="password",
        )
        counting_all_register = Register.objects.all().count()
        self.assertEqual(counting_all_register, 1)

    def test_register_form__validation_for_blank_items(self):
        form = RegisterForm(
            data={
                "name": "",
                "dob": "1999-01-01",
                "email": "ariqharyo29@gmail.com",
                "password": "password",
                "password_verif": "password",
            }
        )
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["name"], ["This field is required."])

    def test_email_validation_fail(self):
        test = "Your email is already registered"
        Register.objects.create(
            name="Igor",
            dob="1999-01-01",
            email="ariqharyo32@gmail.com",
            password="password",
            password_verif="password",
        )

        response_post = Client().post(
            "/register/add_donatur/",
            {
                "name": "Igorrr",
                "dob": "1999-01-01",
                "email": "ariqharyo32@gmail.com",
                "password": "password",
                "password_verif": "password",
            },
        )

        self.assertEqual(Register.objects.filter(
            email="ariqharyo32@gmail.com").count(), 1)
        html_response = response_post.content.decode("utf8")
        self.assertIn(test, html_response)

    def test_register_validation_success(self):
        test = "Registration Success"
        response_post = Client().post(
            "/register/add_donatur/",
            {
                "name": "Igorrr",
                "dob": "1999-01-01",
                "email": "ariqharyo32@gmail.com",
                "password": "password",
                "password_verif": "password",
            },
        )
        self.assertEqual(response_post.status_code, 200)
        html_response = response_post.content.decode("utf8")
        self.assertIn(test, html_response)

    def test_password_validation_fail(self):
        test = "Your password did not match"
        response_post = Client().post(
            "/register/add_donatur/",
            {
                "name": "Igorrr",
                "dob": "1999-01-01",
                "email": "ariqharyo32@gmail.com",
                "password": "password",
                "password_verif": "passwordddd",
            },
        )
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode("utf8")
        self.assertIn(test, html_response)
