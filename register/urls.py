from django.urls import path
from .views import add_donatur, index

urlpatterns = [
    path("register/", index, name="index"),
    path("add_donatur/", add_donatur, name="add_donatur")
]
