from django import forms
from .models import Register


class RegisterForm(forms.ModelForm):
    widgets = {}
    widgets['name'] = forms.TextInput(attrs={"placeholder": "Enter Your Name"})
    widgets['dob'] = forms.DateInput(
        attrs={"placeholder": "yyyy-mm-dd"}, format=('%Y-%m-%d'))
    widgets['email'] = forms.EmailInput(attrs={"placeholder": "Enter Your Email"})
    widgets['password'] = forms.PasswordInput(
        attrs={"placeholder": "Enter Your Password"}, render_value=False)
    widgets['password_verif'] = forms.PasswordInput(
        attrs={"placeholder": "Enter Your Password (again)"}, render_value=False)

    name = forms.CharField(label='Name', widget=widgets['name'])
    dob = forms.DateField(label='Date of Birth',
                          widget=widgets['dob'], input_formats=('%Y-%m-%d',))
    email = forms.EmailField(label='Email', widget=widgets['email'])
    password = forms.CharField(label="Your Password", widget=widgets['password'])
    password_verif = forms.CharField(
        label="Verify Your Password", widget=widgets['password_verif'])

    class Meta:
        model = Register
        fields = [
            "name",
            "dob",
            "email",
            "password",
            "password_verif",
        ]

    def clean_password(self):
        password = self.cleaned_data.get("password")
        password_verif = self.data.get("password_verif")
        if (password != password_verif):
            raise forms.ValidationError("Your password did not match")
        return password_verif

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if not Register.objects.filter(email=email).exists():
            return email
        raise forms.ValidationError("Your email is already registered")
