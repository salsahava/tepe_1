from django.apps import AppConfig


class RegisterTestConfig(AppConfig):
    name = "register"
